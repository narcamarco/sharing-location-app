import React from 'react';
const LoginInput = (props) => {
  const { username, setUsername } = props;

  const handleValueChange = (e) => {
    setUsername(e.target.value);
  };

  return (
    <input
      type="text"
      className="l_page_input"
      placeholder="Enter Your Username"
      value={username}
      onChange={handleValueChange}
    />
  );
};

export default LoginInput;
