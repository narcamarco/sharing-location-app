import store from '../store/store';
import { setLocalStream, setRemoteStream } from './videoRoomsSlice';
import { Peer } from 'peerjs';

let peer;
let peerId;

export const getPeerId = () => {
  return peerId;
};

export const getAccessToLocalStream = async () => {
  const localStream = await navigator.mediaDevices.getUserMedia({
    video: true,
    audio: true,
  });

  if (localStream) {
    console.log(localStream);
    store.dispatch(setLocalStream(localStream));
  }

  return Boolean(localStream);
};

export const connectWithPeerServer = () => {
  peer = new Peer(undefined, {
    host: 'localhost',
    port: 8000,
    path: '/peer',
  });

  peer.on('open', (id) => {
    peerId = id;
  });

  // Receiving the call
  peer.on('call', async (call) => {
    const localStream = store.getState().videoRooms.localStream;
    // Answer the call with A/V stream
    call.answer(localStream);
    call.on('stream', (remoteStream) => {
      store.dispatch(setRemoteStream(remoteStream));
    });
  });
};

// This is the caller Side (Initiating the call)
export const call = (data) => {
  const { newParticipantPeerId } = data;
  const localStream = store.getState().videoRooms.localStream;

  const peerCall = peer.call(newParticipantPeerId, localStream);

  peerCall.on('stream', (remoteStream) => {
    store.dispatch(setRemoteStream(remoteStream));
  });
};

export const disconnect = async () => {
  // const localStream = store.getState().videoRooms.localStream;

  // const audio = localStream.getTracks()[0];
  // const camera = localStream.getTracks()[1];
  // console.log(audio);
  // console.log(camera);

  // audio.stop();
  // camera.stop();

  for (let conns in peer.connections) {
    peer.connections[conns].forEach((c) => {
      c.peerConnection.close();

      if (c.close) {
        c.close();
      }
    });
  }
  store.dispatch(setRemoteStream(null));
};
