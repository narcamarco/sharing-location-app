import React from 'react';
import { useDispatch } from 'react-redux';
import { addChatBoxes } from '../../Messenger/messengerSlice';
import chatIcon from '../../resources/images/chat-icon.svg';

const ChatButton = ({ socketId, username }) => {
  const dispatch = useDispatch();

  const handleAddChatBox = () => {
    dispatch(
      addChatBoxes({
        username,
        socketId,
      })
    );
  };

  return (
    <img
      className="map_page_card_img"
      onClick={handleAddChatBox}
      src={chatIcon}
      alt={username}
    />
  );
};

export default ChatButton;
