import React from 'react';
import ChatBox from './ChatBox/ChatBox';
import { useSelector } from 'react-redux';

import './Messenger.css';

const Messenger = () => {
  const chatBoxes = useSelector((state) => state.messenger.chatBoxes);
  return (
    <div className="messenger_container">
      {chatBoxes.map((chatBox) => {
        return (
          <ChatBox
            key={chatBox.socketId}
            socketId={chatBox.socketId}
            username={chatBox.username}
          />
        );
      })}
    </div>
  );
};

export default Messenger;
