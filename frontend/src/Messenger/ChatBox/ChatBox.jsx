import React from 'react';
import Messages from './Messages';
import Navbar from './Navbar';
import NewMessage from './NewMessage';

const ChatBox = (props) => {
  const { socketId } = props;

  return (
    <div className="chatbox_container">
      <Navbar {...props} />
      <Messages socketId={socketId} />
      <NewMessage socketId={socketId} />
    </div>
  );
};

export default ChatBox;
