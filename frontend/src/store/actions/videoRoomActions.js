import { v4 as uuid } from 'uuid';
import {
  setInRoom,
  setRooms,
} from '../../realtimeCommunication/videoRoomsSlice';
import store from '../store';
import {
  createVideoRoom as createVideoRooms,
  joinVideoRoom,
  leaveVideoRoom as leaveVideoRooms,
} from '../../socketConnection/socketConn';
import {
  disconnect,
  getAccessToLocalStream,
  getPeerId,
} from '../../realtimeCommunication/webRTCHandler';

export const createVideoRoom = async () => {
  const success = await getAccessToLocalStream();

  if (success) {
    const newRoomId = uuid();

    store.dispatch(setInRoom(newRoomId));

    createVideoRooms({
      peerId: getPeerId(),
      newRoomId,
    });
  }
};

export const joinVideoRooms = async (roomId) => {
  const success = await getAccessToLocalStream();

  if (success) {
    store.dispatch(setInRoom(roomId));
    joinVideoRoom({
      roomId,
      peerId: getPeerId(),
    });
  }
};

export const leaveVideoRoom = (roomId) => {
  disconnect();
  leaveVideoRooms({
    roomId,
  });

  store.dispatch(setInRoom(false));
};
export const videoRoomsListHandler = (videoRooms) => {
  store.dispatch(setRooms(videoRooms));
};
