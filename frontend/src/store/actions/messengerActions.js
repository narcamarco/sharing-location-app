import { v4 as uuid } from 'uuid';
import { addChatBoxes, addChatMessage } from '../../Messenger/messengerSlice';
import store from '../store';
import { sendChatMessage as sendChatMessages } from '../../socketConnection/socketConn';

export const sendChatMessage = (receiverSocketId, content) => {
  const message = {
    content,
    receiverSocketId,
    id: uuid(),
  };

  sendChatMessages(message);

  store.dispatch(
    addChatMessage({
      socketId: receiverSocketId,
      content,
      myMessage: true,
      id: message.id,
    })
  );
};

export const chatMessageHandler = (messageData) => {
  store.dispatch(
    addChatMessage({
      socketId: messageData.senderSocketId,
      content: messageData.content,
      myMessage: false,
      id: messageData.id,
    })
  );

  openChatBoxIfClosed(messageData.senderSocketId);
};

export const openChatBoxIfClosed = (socketId) => {
  const chatBox = store
    .getState()
    .messenger.chatBoxes.find((c) => c.socketId === socketId);

  const username = store
    .getState()
    .map.onlineUsers.find((user) => user.socketId === socketId)?.username;
  if (!chatBox) {
    store.dispatch(addChatBoxes({ socketId, username }));
  }
};
