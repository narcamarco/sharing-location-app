import React from 'react';
import callIcon from '../resources/images/call-icon.svg';
import { createVideoRoom } from '../store/actions/videoRoomActions';
import { useSelector } from 'react-redux';

const CreateRoomButton = () => {
  const inRoom = useSelector((state) => state.videoRooms.inRoom);

  const handleRoomCreate = () => {
    if (inRoom) {
      return alert('You are Already In The Room');
    }
    
    createVideoRoom();
  };

  return (
    <img
      src={callIcon}
      alt="Room Button"
      className="map_page_card_img"
      onClick={handleRoomCreate}
    />
  );
};

export default CreateRoomButton;
