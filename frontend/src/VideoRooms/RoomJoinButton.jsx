import React from 'react';
import { useSelector } from 'react-redux';
import { joinVideoRooms } from '../store/actions/videoRoomActions';

const RoomJoinButton = ({ creatorUsername, roomId, amountOfParticipants }) => {
  const inRoom = useSelector((state) => state.videoRooms.inRoom);

  const handleJoinRoom = () => {
    if (inRoom) {
      return alert('Already In The Room');
    }

    if (amountOfParticipants > 1) {
      return alert('Room is Full');
    }
    
    joinVideoRooms(roomId);
  };

  return (
    <button onClick={handleJoinRoom} className="map_page_v_rooms_join_button">
      {creatorUsername[0]}
    </button>
  );
};

export default RoomJoinButton;
